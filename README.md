We have retired this repository.

For the latest information and instructions refer to [https://micro-ros.github.io/](https://micro-ros.github.io/) or to [https://github.com/micro-ROS/micro-ROS-build](https://github.com/micro-ROS/micro-ROS-build)

